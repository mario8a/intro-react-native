import React from 'react'
import { View, Text, Button, SafeAreaView } from 'react-native'

export default function HomeScreen(props) {
  const { navigation } = props;

  const goToPageSettings = () => {
    navigation.navigate("Settings");
  }

  //navigation.goBack(); 

  return (
    <SafeAreaView>
      <Text>Estamos en home screen </Text>
      <Text>Estamos en home screen </Text>
      <Text>Estamos en home screen </Text>
      <Text>Estamos en home screen </Text>
      <Text>Estamos en home screen </Text>
      <Text>Estamos en home screen </Text>
      <Button onPress={goToPageSettings} title='Ir a ajustes'/>
    </SafeAreaView>
  )
}
