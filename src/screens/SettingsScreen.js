import React from 'react'
import { View, Text, Button, SafeAreaView } from 'react-native'

export default function SettingsScreen(props) {
  const { navigation } = props;
  const goToPage = (pageName) => {
    navigation.navigate(pageName);
  }
  return (
    <SafeAreaView>
      <Text>Estamos en settings screen</Text>
      <Text>Estamos en settings screen</Text>
      <Text>Estamos en settings screen</Text>
      <Text>Estamos en settings screen</Text>
      <Text>Estamos en settings screen</Text>
      <Text>Estamos en settings screen</Text>
      <Button onPress={() => goToPage("Home")} title='Ir a HOME'/>
    </SafeAreaView>
  )
}

// Si están probando con Android el SafeAreaView no funciona correctamente.
// expo install react-native-safe-area-context

// y luego importándolo:
// import { SafeAreaView } from “react-native-safe-area-context”;