import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';

export const Saludar = ({name, lastName}) => {
  return (
    <Text> Hola {name} {lastName} </Text>
  )
}


Saludar.defaultProps = {
  name: 'Mario',
  lastName: 'Tovar'
}

Saludar.propTypes = {
  name: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired
}